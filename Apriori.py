import numpy as np
supportValue = 0.3
items = list(np.arange(1, 21))
rands = np.random.randint(1, 10, 20)
transactions = np.array([np.random.choice(items, x) for x in rands], dtype=object)

def generateC(dataset):
    c1 = []
    for data in dataset:
        for item in data:
            if [item] not in c1:
                c1.append([item])
                
    return list(map(frozenset, c1))

def scanDataSet(D, Ck, minSupport):
    ssCnt = {}
    for tid in D:
        for can in Ck:
            if can.issubset(tid):
                if not can in ssCnt: ssCnt[can]=1
                else: ssCnt[can] += 1
    numItems = float(len(D))
    retList = []
    supportData = {}
    for key in ssCnt:
        support = ssCnt[key]/numItems
        if support >= minSupport:
            retList.insert(0,key)
        supportData[key] = support
    return retList, supportData

def GenerateNextItemSet(Lk, k): 
    retList = []
    lenLk = len(Lk)
    for i in range(lenLk):
        for j in range(i+1, lenLk): 
            L1 = list(Lk[i]);L1 = L1[:k-2]; L1.sort();
            L2 = list(Lk[j])[:k-2]; L2.sort();
            if L1==L2:
                retList.append(Lk[i] | Lk[j]) 
    return retList

def apriori(dataSet, minSupport = 0.2):
    C1 = generateC(dataSet)
    D = list(map(set, dataSet))
    L1, supportData = scanDataSet(D, C1, minSupport)
    L = [L1]
    k = 2
    while (len(L[k-2]) > 0):
        Ck = GenerateNextItemSet(L[k-2], k)
        Lk, supK = scanDataSet(D, Ck, minSupport)
        supportData.update(supK)
        L.append(Lk)
        k += 1
    return L, supportData


def calcConf(freqSet, H, supportData, brl, minConf=0.7):
    prunedH = [] 
    for conseq in H:
        conf = supportData[freqSet]/supportData[freqSet-conseq] #calc confidence
        if conf >= minConf : 
            print (*freqSet-conseq,'-->',*conseq,'conf:',conf)
            brl.append((freqSet-conseq, conseq, conf))
            prunedH.append(conseq)
    return prunedH

def rulesFromConseq(freqSet, H, supportData, brl, minConf=0.7):
    m = len(H[0])
    if (len(freqSet) > (m + 1)): 
        Hmp1 = GenerateNextItemSet(H, m+1)
        Hmp1 = calcConf(freqSet, Hmp1, supportData, brl, minConf)
        if (len(Hmp1) > 1):    
            rulesFromConseq(freqSet, Hmp1, supportData, brl, minConf)
            
def generateRules(L, supportData, minConf=0.7):  
    bigRuleList = []
    for freqSet in L[-2]:
        H1 = [frozenset([item]) for item in freqSet]
        rulesFromConseq(freqSet, H1, supportData, bigRuleList, minConf)        
    return bigRuleList



L, Supp = apriori(transactions, 0) 
print("Transactions")
for t in transactions :
    print(t)

print("\nAsscociation Rules:")
rules= generateRules(L,Supp, minConf=0.1)


